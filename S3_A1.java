package zuitt.com;
import java.util.Scanner;

public class S3_A1 {
    public static void main(String[] args) {

        // START STRETCH GOALS
            int rows = 5;

            for (int i = 1; i <= rows; i++) {
                for (int j = 1; j <= i; j++) {
                    System.out.print("* ");
                }
                System.out.println();
            }
        // END STRETCH GOALS

        int num = 0;

        try {
            Scanner inputInt = new Scanner(System.in);
            System.out.println("Input an integer whose factorial will be computed:");
            num = inputInt.nextInt();

            if (num < 0) {
                System.out.println("Negative number are not allowed");
            }
        } catch (Exception e) {
            System.out.println("Invalid input: " + e.getMessage());
            System.exit(0);
        }

        int factorial = calculateFactorial(num);
        System.out.println(("The Factorial of " + num + " is " + factorial));
    }
        public static int calculateFactorial(int n) {
            if (n == 0 || n == 1) {
                return 1;
            }

            int factorial = 1;

            for (int i = 1; i <= n; i++) {
                factorial *= i;
            }
            return factorial;
        }


}
